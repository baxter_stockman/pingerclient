module.exports = {
  "extends": ["eslint:recommended", "google"],
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module",
    "ecmaFeatures": {
      "experimentalObjectRestSpread": true,
      "jsx": true
    }
  },
  "plugins": [
     "react"
   ],
  "rules": {
        "no-undef": "off",
        "no-unused-vars": "off",
        "object-curly-spacing": ["warn", "always"],
        "quotes": ["warn", "single", { "allowTemplateLiterals": true }],
        "brace-style": ["warn", "1tbs", { "allowSingleLine": true }],
        "semi": ["warn", "always", { "omitLastInOneLineBlock": true}],
        "require-jsdoc": "off",
        "no-console": "off",
    },
};
