import { combineReducers } from 'redux';
import { asyncReducer } from './asyncReducer';
import { syncReducer } from './syncReducer';
import { reducer as formReducer } from 'redux-form';

const mainReducer = combineReducers({
  async: asyncReducer,
  sync: syncReducer,
  form: formReducer,
});

export { mainReducer };
