import { List as immutableList, Map as immutableMap } from 'immutable';

const defaultState = {
  loggedIn: false,
  sites: immutableList([]),
};

export const syncReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'LOGIN':
    return {
      loggedIn: true,
      sites: state.sites,
    };
    case 'LOGOUT':
    return defaultState;
    case 'ADD_SITE':
    return state.update('sites', (sites) => {
      return sites.includes(action.url) ? sites : sites.push(action.url);
    });
    case 'REMOVE_SITE':
    return state.update('sites', (sites) => {
      return sites.has(action.index) ? sites.delete(action.index) : sites;
    });
    default:
    return state;
  }
};
