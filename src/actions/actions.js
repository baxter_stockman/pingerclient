
export const addSite = (url) => ({ type: 'ADD_SITE', url });

export const removeSite = (index) => ({ type: 'REMOVE_SITE', index });

export const login = () => ({ type: 'LOGIN' });

export const logout = () => ({ type: 'LOGOUT' });

export const startLocationFetch = () => ({ type: 'START_LOCATION_FETCH' });

export const completeLocationFetch = (url) => ({
  type: 'COMPLETE_LOCATION_FETCH',
  url,
});

export const fetchLocation = () => {
  return (dispatch, getState) => {
    dispatch(startLocationFetch());
    fetch('http://ipinfo.io').then((response) => {
      const loc = response.json().loc;
      const baseUrl = 'http://maps.google.com?q=';
      dispatch((completeLocationFetch(baseUrl + loc)));
    });
  };
};

export const Actions = { addSite, removeSite, login, logout };
