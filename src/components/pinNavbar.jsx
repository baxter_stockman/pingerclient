import * as React from 'react';
import { Component, render } from 'react-dom';
import { history, withRouter } from 'react-router';
import { Link } from 'react-router-dom';

import { Navbar } from 'react-bootstrap';

const pinNavbar = ({ title }) => (
  <Navbar>
    <Navbar.Header>
      <Navbar.Brand>
        <Link to="/">{title}</Link>
      </Navbar.Brand>
    </Navbar.Header>
  </Navbar>
  );

export { pinNavbar as PinNavbar };
