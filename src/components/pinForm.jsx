import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import { Field, reduxForm } from 'redux-form';
import { Button,
         FormGroup,
         FormControl,
         HelpBlock,
         ControlLabel } from 'react-bootstrap';

import { EMAIL_PATTERN } from '../utils/email.pattern.js';

class PinForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = { email: '', password: '' };
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.getEmailValidationState = this.getEmailValidationState.bind(this);
    this.getPasswordValidationState =
        this.getPasswordValidationState.bind(this);
    this.submitWrapper = this.submitWrapper.bind(this);
  }

  submitWrapper(evt) {
    evt.preventDefault();
    this.props.onSubmit({
      email: findDOMNode(this.refs['email']).value,
      password: findDOMNode(this.refs['password']).value,
    });
  }

  getEmailValidationState() {
    const email = this.state.email;
    // if (length > 10) return 'success';
    // else if (length > 5) return 'warning';
    // else if (length > 0) return 'error';
    return EMAIL_PATTERN.test(email) ? 'success' : 'error';
  }

  getPasswordValidationState() {
    const length = this.state.password.length;
    // if (length > 10) return 'success';
    // else if (length > 5) return 'warning';
    // else if (length > 0) return 'error';;
    return length > 5 ? 'success' : 'error';
  }

  handleEmailChange(e) {
    this.setState({ ...this.state, email: e.target.value });
  }

  handlePasswordChange(e) {
    this.setState({ ...this.state, password: e.target.value });
  }

  render() {
    const { handleSubmit, buttonLabel, pristine } = this.props;
    return (
      <form onSubmit={this.submitWrapper}>
        <FormGroup
          controlId="emailField"
          validationState={this.getEmailValidationState()}
          >
          <ControlLabel>User email</ControlLabel>
          <FormControl ref="email"
            type="email"
            placeholder="johndoe@example.com"
            value={this.state.email}
            onChange={this.handleEmailChange}
            />
          <FormControl.Feedback />
          <HelpBlock>
            {this.getEmailValidationState() !== 'success' ?
             'Valid Email required'
             : 'Email OK'}
          </HelpBlock>
          </FormGroup>
          <FormGroup
            controlId="passwordField"
            validationState={this.getPasswordValidationState()}
            >
          <ControlLabel>Your Password</ControlLabel>
          <FormControl
            type="password" ref="password"
            value={this.state.password}
            onChange={this.handlePasswordChange}
            placeholder="YOurStrongPassw0rd"
            />
          <FormControl.Feedback />
          <HelpBlock>
            {this.getPasswordValidationState() !== 'success' ?
             'Password should be at least 6 charactes long'
             : 'Password OK'}
          </HelpBlock>
        </FormGroup>
        <Button type="submit"
          disabled={this.getEmailValidationState() !== 'success'
            || this.getPasswordValidationState() !== 'success'}>
          Submit
        </Button>
      </form>
    );
  }
}

// eslint-disable-next-line no-class-assign
PinForm = reduxForm({
  form: 'pinForm', // object key inside state tree with unique ID
})(PinForm);

export { PinForm };
