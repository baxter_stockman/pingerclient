import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button,
         ControlLabel,
         Col,
         Form,
         FormGroup,
         FormControl,
         HelpBlock,
         Row } from 'react-bootstrap';

import { UrlItem } from './urlItem';

const dummySites = [
   {
     url: 'www.google.rs',
     interval: 32,
   },
   {
     url: 'asus.tw',
     interval: 543,
   },
   {
     url: 'www.amd.com',
     interval: 42,
}];

class PinSites extends React.Component {
   constructor(props) {
     super(props);
   }

   submitWrapper(evt) {
     evt.preventDefault();
     this.props.onSubmit({
       one: 'Alsa',
       two: 'Mixer',
     });
   }

   render() {
     const { handleSubmit, sumbitLabel, pristine } = this.props;
     return (
       <Form inline onSubmit={this.submitWrapper}>
         <UrlItem url="http://test.com" />
         <UrlItem url="https://www.high.co.jp" />
       </Form>
     );
   }
}

// eslint-disable-next-line no-class-assign
PinSites = reduxForm({
  form: 'pinSites', // object key inside state tree with unique ID
})(PinSites);

export { PinSites };
