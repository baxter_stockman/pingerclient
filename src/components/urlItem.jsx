import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button,
         DropdownButton,
         ControlLabel,
         FormGroup,
         FormControl,
         HelpBlock,
         MenuItem,
       } from 'react-bootstrap';

class UrlItem extends React.Component {
   constructor(props) {
     super(props);
     const url = props.url;
     const connectionRe = /https?\:\/\//;
     console.log(url.match(/https?\:\/\//));
     this.state = { connectionType: 'http://' };
     this.submitWrapper = this.submitWrapper.bind(this);
     this.handleConnectionType = this.handleConnectionType.bind(this);
   }

   handleConnectionType(key, evt) {
         if (key === '1') {
           this.setState({ connectionType: 'http://' });
         } else {
           this.setState({ connectionType: 'https://' });
         }
   }

   submitWrapper(evt) {
     evt.preventDefault();
     this.props.onSubmit({
       one: 'Alsa',
       two: 'Mixer',
     });
   }

   render() {
     return (
         <FormGroup controlId="urlField" >
           <DropdownButton onSelect={this.handleConnectionType}
             style={{ margin: '5px', width: '80px' }}
             title={this.state.connectionType} id="dropdown-size-medium">
             <MenuItem eventKey="1">'http://'</MenuItem>
             <MenuItem divider />
             <MenuItem eventKey="2">'https://'</MenuItem>
           </DropdownButton>
           <FormControl ref="urlField"
             type="text"
             placeholder="example.com"
             />
           <FormControl.Feedback />
             <Button style={{ margin: '5px' }} type="submit">Add site</Button>
           </FormGroup>
     );
   }
}

export { UrlItem };
