import * as React from 'react';
import { Component, render } from 'react-dom';
import { history, withRouter } from 'react-router';
import { Col, Row, Grid, Button } from 'react-bootstrap';

const outerContainerStyle = {
  display: 'flex',
  flexFlow: 'row wrap',
};

const innerContainerStyle = {
  flex: '1 1 50%',
  display: 'flex',
  justifyContent: 'center',
};

const home = withRouter(({ history }) => (
  <div style={outerContainerStyle}>
    <div style={innerContainerStyle}>
      <Button bsStyle="primary" bsSize="large"
        onClick={() => history.push('/login')}>Login</Button>
    </div>
    <div style={innerContainerStyle}>
      <Button bsStyle="info" bsSize="large"
        onClick={() => history.push('/signup')}>Signup</Button>
    </div>
  </div>
));

export { home as Home };
