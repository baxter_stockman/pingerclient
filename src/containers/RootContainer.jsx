import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router,
  Route,
  Link,
  withRouter } from 'react-router-dom';
import { Col, Row, Grid, Navbar, Panel } from 'react-bootstrap';

import { PinNavbar } from '../components/pinNavbar';
import { Home } from '../components/home';
import { PinSites } from '../components/pinSites';
import { PinForm } from '../components/pinForm';
import '../reducers/mainReducer';
import { login } from '../actions/actions';

let RootContainer = ({ loggedIn, dispatchLogin }) => (
  <Router>
    <Grid>
      <Row className="show-grid">
        <PinNavbar title="Pinger App" />
        <Col xs={6} xsOffset={3}>
          <Panel>
            <Link to="/sites">Sites</Link>
            <Route exact path="/" component={Home} />
            <Route exact path="/login"
              render={() => <PinForm buttonLabel="login"
                onSubmit={(state) => console.log(state)}/>}/>
            <Route exact path="/signup"
              render={() => <PinForm buttonLabel="signup"
                onSubmit={(state) => console.log(state)}/>}/>
              <Route exact path="/sites" render={() => <PinSites />}/>
          </Panel>
        </Col>
      </Row>
    </Grid>
  </Router>
);

const mapStateToProps = (state) => ({ loggedIn: state.sync.loggedIn });
const mapDispatchToProps = (dispatch) => ({
  dispatchLogin: () => dispatch(login()),
});

RootContainer = connect(mapStateToProps, mapDispatchToProps)(RootContainer);

export { RootContainer };
