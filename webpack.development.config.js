const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OpenerPlugin = require('webpack-opener');

// const extractSass = new ExtractTextPlugin({
//   filename: '[name].[contenthash].css',
//   disable: process.env.NODE_ENV === 'development',
// });
//
// console.log(process.env.NODE_ENV === 'development');
module.exports = {
  entry: [
    // activate HMR for React
    'react-hot-loader/patch',

    // bundle the client for webpack-dev-server
    // and connect to the provided endpoint
    'webpack-dev-server/client?http://localhost:4000/',

    // bundle the client for hot reloading
    // only- means to only hot reload for successful updates
    'webpack/hot/only-dev-server',

    // fetch polyfill
    'whatwg-fetch',
    // the app entry point
    path.resolve(__dirname, 'src', 'index.jsx'),
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/dist/',
  },

  devServer: {
    // enable HMR on the server
    hot: true,

    // match the output path
    contentBase: path.resolve(__dirname, './'),

    // match the output `publicPath`
    publicPath: '/dist/',

    // port
    port: 3000,

    // history api support
    historyApiFallback: true,

    // report only errors
    stats: 'errors-only',

    // show errors and warnings in overlay
    overlay: {
      errors: true,
      warnings: true,
    },

  },

  // Enable sourcemaps for debugging webpack's output.
  devtool: 'source-map',

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: ['babel-loader'],
        exclude: /node_modules/,
      },
      {
        enforce: 'pre',
        test: /\.jsx?$/,
        use: ['source-map-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.s?css$/,
        // use: [{
        //   loader: 'style-loader', // creates style nodes from JS strings
        // }, {
        //   loader: 'css-loader', // translates CSS into CommonJS
        // }, {
        //   loader: 'sass-loader', // compiles Sass to CSS
        // }],
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          // resolve-url-loader may be chained before sass-loader if necessary
          use: ['css-loader', 'sass-loader'],
        }),
      },
      // {
      //   test: /\.css$/,
      //   use: ExtractTextPlugin.extract({
      //   // resolve-url-loader may be chained before sass-loader if necessary
      //     use: ['css-loader'],
      //   }),
      // },
      {
      test: /\.(png|woff|woff2|eot|ttf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      loader: 'url-loader',
      options: {
        limit: 25000,
      },
    },
    ],
  },
  resolve: {
    // Add '.js' and '.jsx' as resolvable extensions.
    extensions: ['.js', '.jsx'],
  },
  // When importing a module whose path matches one of the following, just
  // assume a corresponding global variable exists and use that instead.
  // This is important because it allows us to avoid bundling all of our
  // dependencies, which allows browsers to cache those libraries between
  // builds.
  // externals: {
  //     'react': 'React',
  //     'react-dom': 'ReactDOM',
  // },
  plugins: [
    // enable HMR globally
    new webpack.HotModuleReplacementPlugin(),

    // prints more readable module names in the browser console on HMR updates
    new webpack.NamedModulesPlugin(),

    // extract css in production
    // extractSass,
    new ExtractTextPlugin('style.css'),
    // open browser
    new OpenerPlugin('http://localhost:3000'),
  ],
};
