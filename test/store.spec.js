import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { List as immutableList, Map as immutableMap } from 'immutable';
import { asyncReducer } from '../src/reducers/asyncReducer';
import { syncReducer } from '../src/reducers/syncReducer';
import { Actions } from '../src/actions/actions';

test('Login Action', () => {
  const initialState = immutableMap({
    loggedIn: false,
    sites: immutableList([]),
  });

  const finalState = immutableMap({
    loggedIn: true,
    sites: immutableList([]),
  });

  const store = createStore(syncReducer, initialState, applyMiddleware(thunk));
  store.dispatch(Actions.login());
  expect(store.getState()).toEqual(finalState);
});

test('Logout Action', () => {
  const initialState = immutableMap({
    loggedIn: true,
    sites: immutableList([]),
  });

  const finalState = immutableMap({
    loggedIn: false,
    sites: immutableList([]),
  });

  const store = createStore(syncReducer, initialState);
  store.dispatch(Actions.logout());
  expect(store.getState()).toEqual(finalState);
});

test('Add Site Action', () => {
  const initialState = immutableMap({
    loggedIn: true,
    sites: immutableList([]),
  });

  const finalState = immutableMap({
    loggedIn: true,
    sites: immutableList(['https://www.google.rs/']),
  });

  const store = createStore(syncReducer, initialState);
  store.dispatch(Actions.addSite('https://www.google.rs/'));
  expect(store.getState()).toEqual(finalState);
});

test('Remove Site Action', () => {
  const initialState = immutableMap({
    loggedIn: true,
    sites: immutableList(['https://www.google.rs/',
    'https://www.asus.tw/',
    'https://www.msi.eu/']),
  });

  const finalState = immutableMap({
    loggedIn: true,
    sites: immutableList(['https://www.google.rs/',
    'https://www.msi.eu/']),
  });

  const store = createStore(syncReducer, initialState);
  store.dispatch(Actions.removeSite(1));
  expect(store.getState()).toEqual(finalState);
});

test('Should not allow same url multiple times in site list', () => {
  const initialState = immutableMap({
    loggedIn: true,
    sites: immutableList(['https://www.google.rs/',
    'https://www.msi.eu/']),
  });

  const store = createStore(syncReducer, initialState);
  store.dispatch(Actions.addSite('https://www.google.rs/'));
  store.dispatch(Actions.addSite('https://www.msi.eu/'));
  expect(store.getState()).toEqual(initialState);
});

test('Should return same state if try to remove out of bounds index', () => {
  const initialState = immutableMap({
    loggedIn: true,
    sites: immutableList(['https://www.google.rs/',
    'https://www.msi.eu/']),
  });

  const store = createStore(syncReducer, initialState);
  store.dispatch(Actions.removeSite(3));
  expect(store.getState()).toEqual(initialState);
});

const mockAsync = () => {
  return new Promise((resolve) => {
    setTimeout(() => resolve('fake url'), 1000);
  });
};

test('Async actions', (done) => {
  const asyncAction = () => {
    return (dispatch, getState) => {
      dispatch({ type: 'START_LOCATION_FETCH' });
      console.log(getState());
      mockAsync().then((res) => {
        dispatch({ type: 'COMPLETE_LOCATION_FETCH', url: res });
        console.log(getState());
        done();
      });
    };
  };

  const store = createStore(asyncReducer, applyMiddleware(thunk));
  store.dispatch(asyncAction());
});
